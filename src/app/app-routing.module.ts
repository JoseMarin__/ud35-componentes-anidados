import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './Home/home/home.component';
import { AboutComponent } from './About/about/about.component';
import { InfoComponent } from './Info/info/info.component';
import { PersonaComponent } from './Persona/persona/persona.component';
import { AddComponent } from './Persona/add/add.component';
import { ListarComponent } from './Persona/listar/listar.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  {path: 'home', component:HomeComponent},
  {path: 'about', component:AboutComponent},
  {path: 'persona', component:PersonaComponent},
  {path: 'add', component:AddComponent},
  {path: 'listar', component:ListarComponent},
  {path: '404', component: InfoComponent},
  {path: '**', redirectTo: '/404'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
