import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HolaHomeComponent } from './hola-home.component';

describe('HolaHomeComponent', () => {
  let component: HolaHomeComponent;
  let fixture: ComponentFixture<HolaHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HolaHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HolaHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
