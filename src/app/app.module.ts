import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './Home/home/home.component';
import { HolaHomeComponent } from './Home/hola-home/hola-home.component';
import { AboutComponent } from './About/about/about.component';
import { InfoComponent } from './Info/info/info.component';
import { NotFoundComponent } from './Info/not-found/not-found.component';
import { PersonaComponent } from './Persona/persona/persona.component';
import { AddComponent } from './Persona/add/add.component';
import { ListarComponent } from './Persona/listar/listar.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HolaHomeComponent,
    AboutComponent,
    InfoComponent,
    NotFoundComponent,
    PersonaComponent,
    AddComponent,
    ListarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
